%
% Lenna - all close and personal
%

clc;
clear

resultsDir = './report/figures/';
fileID = fopen('./input/lotr.txt', 'r');
message = fscanf(fileID, '%c');


% lena = rgb2gray(imread('./input/Images/lena.png'));

% OPTION 1: Mandril
image = rgb2gray(imread('./input/Images/standard_test_images/mandril_color.tif'));

% OPTION 2: Lena
% image = imread('./input/Images/standard_test_images/lena_gray_512.tif');

if(size(size(image), 2) == 3)
    % If image is color make it black and white
    image = rgb2gray(image);
end

if(size(image, 1) ~= 512 || size(image, 2) ~= 512)
    % If image has wrong dimensions
    disp('Resizing the image')
    image = imresize(image,[512 512]);
end
% imshow(image)

%%
lena_contig = zeros(512,512,7);
lena_spread = zeros(512,512,7);

lena_contig_MSE = [];
lena_contig_PSNR = [];
lena_contig_SSI = [];

lena_spread_MSE = [];
lena_spread_PSNR = [];
lena_spread_SSI = [];

for bps =1:7
   lena_contig(:,:,bps) = image2steg(image, message, bps);
   lena_contig_MSE(bps) = mse(uint8(lena_contig(:,:,bps)), image);
   lena_contig_PSNR(bps) = PSNR(uint8(lena_contig(:,:,bps)), image, 255);
   lena_contig_SSI(bps) = ssim(uint8(lena_contig(:,:,bps)), image);
   
   lena_spread(:,:,bps) = image2SpreadSteg(image, message, bps);
   lena_spread_MSE(bps) = mse(uint8(lena_spread(:,:,bps)), image);
   lena_spread_PSNR(bps) = PSNR(uint8(lena_spread(:,:,bps)), image, 255);
   lena_spread_SSI(bps) = ssim(uint8(lena_spread(:,:,bps)), image);
end

%% Plot the contiguous vs spread 
figure();
set(gcf,'position',[10,10,1200,300]);
subplot(1,3,1);
set(gca, 'FontSize', 16, 'FontName', 'Times');
hold on;
    plot(lena_contig_MSE, '-dr');
    plot(lena_spread_MSE, '-db');
hold off;
set(gca, 'xtick', 1:7);
leg = legend('Contiguous Embedding', 'Spread Embedding');
leg.FontSize = 16;
leg.Location = 'NorthWest';
% title('Contiguous vs spread bit embedding on the steganographic image''s mean squared error (MSE).');
xlabel('Bits per Pixel');
ylabel('MSE');


subplot(1,3,2);
set(gca, 'FontSize', 16, 'FontName', 'Times');
hold on;
    plot(lena_contig_PSNR, '-dr');
    plot(lena_spread_PSNR, '-db');
hold off;
set(gca, 'xtick', 1:7);
leg = legend('Contiguous Embedding', 'Spread Embedding');
leg.FontSize = 16;
leg.Location = 'NorthEast';
% title('Contiguous vs spread bit embedding on the steganographic image''s peak signal to noise ratio (PSNR).');
xlabel('Bits per Pixel');
ylabel('PSNR (dB)');


subplot(1,3,3);
set(gca, 'FontSize', 16, 'FontName', 'Times');
hold on;
    plot(lena_contig_SSI, '-dr');
    plot(lena_spread_SSI, '-db');
hold off;
set(gca, 'xtick', 1:7);
leg = legend('Contiguous Embedding', 'Spread Embedding');
leg.FontSize = 16;
leg.Location = 'SouthWest';
% title('Contiguous vs spread bit embedding on the steganographic image''s strutural similiarity index (SSI).');
xlabel('Bits per Pixel');
ylabel('SSI');

saveas(gcf, strcat(resultsDir, 'lenaAnal.png'), 'png');

%%
figure();
subplot(2,4,1), imshow(uint8(lena_spread(:,:,1))); title('1 Embedded Bit'); set(gca, 'FontSize', 14);
diff = abs(image - uint8(lena_spread(:,:,1)));
subplot(2,4,5), imshow(250.-(80.*(diff))); title('Difference Map'); set(gca, 'FontSize', 14);

subplot(2,4,2), imshow(uint8(lena_spread(:,:,2))); title('2 Embedded Bit'); set(gca, 'FontSize', 14);
diff = abs(image - uint8(lena_spread(:,:,2)));
% axes('Position',[0 0 1 1],'xtick',[],'ytick',[],'box','on','handlevisibility','off');
subplot(2,4,6), imshow(250.-(60.*(diff))); title('Difference Map'); set(gca, 'FontSize', 14);

subplot(2,4,3), imshow(uint8(lena_spread(:,:,3))); title('3 Embedded Bit'); set(gca, 'FontSize', 14);
diff = abs(image - uint8(lena_spread(:,:,3)));
% axes('Position',[0 0 1 1],'xtick',[],'ytick',[],'box','on','handlevisibility','off');
subplot(2,4,7), imshow(250.-(80.*(diff))); title('Difference Map'); set(gca, 'FontSize', 14);

subplot(2,4,4), imshow(uint8(lena_spread(:,:,4))); title('4 Embedded Bits'); set(gca, 'FontSize', 14);
diff = abs(image - uint8(lena_spread(:,:,4)));
% axes('Position',[0 0 1 1],'xtick',[],'ytick',[],'box','on','handlevisibility','off');
subplot(2,4,8), imshow(250.-(80.*(diff))); title('Difference Map'); title('Difference Map'); set(gca, 'FontSize', 14);


%%
figure(1)
subplot(1,4,1), imshow(uint8(lena_spread(:,:,1))); title('1 Bit Per Pixel'); set(gca, 'FontSize', 20);
diff = abs(image - uint8(lena_spread(:,:,1)));
subplot(1,4,2), imshow(250.-(80.*(diff))); title('Difference Map'); set(gca, 'FontSize', 20);

subplot(1,4,3), imshow(uint8(lena_spread(:,:,7))); title('7 Bit Per Pixel'); set(gca, 'FontSize', 20);
diff = abs(image - uint8(lena_spread(:,:,7)));
subplot(1,4,4), imshow(250.-(80.*(diff))); title('Difference Map'); set(gca, 'FontSize', 20);

%%
figure()
subplot(1,2,1), imshow(uint8(lena_spread(:,:,7))); title('7 Bits Per Pixel'); set(gca, 'FontSize', 14);

subplot(1,2,2), imshow(uint8(lena_contig(:,:,7))); title('7 Bits Per Pixel'); set(gca, 'FontSize', 14);

%%
figure()
imshow(uint8(lena_spread(:,:,1)));
figure()
imshow(uint8(lena_spread(:,:,5)));
figure()
imshow(uint8(lena_spread(:,:,7)));