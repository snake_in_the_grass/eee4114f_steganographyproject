% ------------------------------------------------------------------------_
% PARAMETERS:
% - image:          uint8, rgb image matrix
% - message:        string to embed in the image
% - bitPackageSize: number of bits per pixel to embed
% 
% RETURNS:
% - steg:           the modified image with message embedded throughout the
%                   entire image
% ------------------------------------------------------------------------_
function [ steg ] = image2SpreadSteg( image, message, bit_package_size )

    % Copy the original image
    steg = image;
    [rows, cols] = size(steg);
    
    
    % Message to bitstream
    s = str2bitstream(message);
%     fprintf('Size of  bit string to embed: %d\n', length(s))
       
    % Pad s for bit package size
    if(mod(length(s), bit_package_size) ~= 0)
        
        padding_level = bit_package_size - mod(length(s), bit_package_size);
        padding = '';
%         fprintf('Bit package size of %d requires padding of: %d\n',bit_package_size, padding_level);

        for i = 1:padding_level
            padding = strcat(padding, '0');
        end
        s = [s,padding]; 
    end
    

    bits_to_embed = length(s);
    message_length = dec2bin(bits_to_embed, 16);    
    s = [message_length s];
        
    if(bits_to_embed > rows * cols)
        disp('Cannot embed all the bits!');
        return;
    end
    
    bits_embedded = 0;          % counts number of bits already embedded
    bit_counter = 1;            % indexes the bit stream
    encoded = false;
    bitdensity = floor((rows*cols)/bits_to_embed)*bit_package_size;     % space between each embedding
    spaces = 0;
    
    % Embed the size of the embedded message first bit by bit
    for j = 1:16
        % Bit package to embed
        bit_package = s(bit_counter);
        bit_counter = bit_counter + 1;

        % Embed the bit package
        steg(1,j) = embedBitPackage(bit_package, steg(1,j));
        bits_embedded = bits_embedded + 1;
    end
    
    for j = 17:cols
        spaces = spaces + 1;
            if(bits_embedded >= bits_to_embed+16 )
                encoded = true;
                break ;
            end
            if(spaces == bitdensity)
                % Bit package to embed
                 bit_package = s(bit_counter:bit_counter+(bit_package_size-1));
                 bit_counter = bit_counter + bit_package_size;
                % Embed the bit package
                 steg(1,j) = embedBitPackage(bit_package, steg(1,j));
                 bits_embedded = bits_embedded + bit_package_size;
                 spaces = 0;
            end         
    end
    
    for i=2:rows
        for j=1:cols
            spaces = spaces + 1;
            if(bits_embedded >= bits_to_embed+16 )
                encoded = true;
                break ;
            end
            if(spaces == bitdensity)
                % Bit package to embed
                 bit_package = s(bit_counter:bit_counter+(bit_package_size-1));
                 bit_counter = bit_counter + bit_package_size;
                % Embed the bit package
                 steg(i,j) = embedBitPackage(bit_package, steg(i,j));               
                 bits_embedded = bits_embedded + bit_package_size;
                 spaces = 0;
            end 
        end
        if(encoded)
            break;
        end
    end
    
    
    
    


end

