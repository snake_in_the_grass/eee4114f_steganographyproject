%
% Genearal
%

clc;
clear;
resultsDir = './report/figures/';
fileID = fopen('./input/accents.txt', 'r');
message = fscanf(fileID, '%c');

% % OPTION 1: Mandril
% image = rgb2gray(imread('./input/Images/standard_test_images/mandril_color.tif'));
% 
% % OPTION 2: Peppers
% % image = imread('peppers.png');
% 
% if(size(size(image), 2) == 3)
%     % If image is color make it black and white
%     image = rgb2gray(image);
% end
% 
% if(size(image, 1) ~= 512 || size(image, 2) ~= 512)
%     % If image has wrong dimensions
%     disp('Resizing the image')
%     image = imresize(image,[512 512]);
% end
% % imshow(image)

%%
% imname = 'mandril_Anal';

% Gray Image
gimage = uint8(ones(512,512));
gimage = 128.*gimage;
gimname = 'gray_Anal.png';

% B&W
bimage = uint8(zeros(512,512));
for i=1:512
    for j =1:512
        if(mod(j,2)==0)
            bimage(i,j)=255;
        end
        if(mod(i,2)==0)
            bimage(i,j)=255;
        end
        if(mod(j,2)==0 && mod(i,2) ==0)
            bimage(i,j)=0;
        end
    end
end
bimname = 'b&wAnal.png';

%%
bimage_contig = zeros(512,512,7);
bimage_spread = zeros(512,512,7);

bimage_contig_MSE = [];
bimage_contig_PSNR = [];
bimage_contig_SSI = [];

bimage_spread_MSE = [];
bimage_spread_PSNR = [];
bimage_spread_SSI = [];


%%

for bps =1:7
   bimage_contig(:,:,bps) = image2steg(bimage, message, bps);
   bimage_contig_MSE(bps) = mse(uint8(bimage_contig(:,:,bps)), bimage);
   bimage_contig_PSNR(bps) = PSNR(uint8(bimage_contig(:,:,bps)), bimage, 255);
   bimage_contig_SSI(bps) = ssim(uint8(bimage_contig(:,:,bps)), bimage);
   
   bimage_spread(:,:,bps) = image2SpreadSteg(bimage, message, bps);
   bimage_spread_MSE(bps) = mse(uint8(bimage_spread(:,:,bps)), bimage);
   bimage_spread_PSNR(bps) = PSNR(uint8(bimage_spread(:,:,bps)), bimage, 255);
   bimage_spread_SSI(bps) = ssim(uint8(bimage_spread(:,:,bps)), bimage);
end

%% Plot the contiguous vs spread 
figure();
set(gcf,'position',[10,10,1200,300]);
subplot(1,3,1);
set(gca, 'FontSize', 16, 'FontName', 'Times');
hold on;
    plot(bimage_contig_MSE, '-dr');
    plot(bimage_spread_MSE, '-db');
hold off;
set(gca, 'xtick', 1:7);
leg = legend('Contiguous Embedding', 'Spread Embedding');
leg.FontSize = 16;
leg.Location = 'NorthWest';
% title('Contiguous vs spread bit embedding on the steganographic image''s mean squared error (MSE).');
xlabel('Bits per Pixel');
ylabel('MSE')
% saveas(gcf, strcat(resultsDir, imname, '1' ), 'png');


subplot(1,3,2);
set(gca, 'FontSize', 16, 'FontName', 'Times');
hold on;
    plot(bimage_contig_PSNR, '-dr');
    plot(bimage_spread_PSNR, '-db');
hold off;
set(gca, 'xtick', 1:7);
leg = legend('Contiguous Embedding', 'Spread Embedding');
leg.FontSize = 16;
leg.Location = 'NorthEast';
% title('Contiguous vs spread bit embedding on the steganographic image''s peak signal to noise ratio (PSNR).');
xlabel('Bits per Pixel');
ylabel('PSNR (dB)');
% saveas(gcf, strcat(resultsDir, imname, '2' ), 'png');

subplot(1,3,3);
set(gca, 'FontSize', 16, 'FontName', 'Times');
hold on;
    plot(bimage_contig_SSI, '-dr');
    plot(bimage_spread_SSI, '-db');
hold off;
set(gca, 'xtick', 1:7);
leg = legend({['Contiguous' 10 'Embedding'], ['Spread Embedding']});  % also good
% legend('Contiguous\n Embedding', 'Spread Embedding');

leg.FontSize = 16;
leg.Location = 'SouthWest';
% title('Contiguous vs spread bit embedding on the steganographic image''s strutural similiarity index (SSI).');
xlabel('Bits per Pixel');
ylabel('SSI');
% saveas(gcf, strcat(resultsDir, imname, '3' ), 'png');


% saveas(gcf, strcat(resultsDir, imname), 'png');

%%



%%
figure('visible', 'off')
imshow(uint8(image(30:130,135:235)));
% saveas(gcf, strcat(resultsDir, 'mandril_0'), 'png');

figure('visible', 'off')
% subplot(2,2,1)
imshow(uint8(bimage_spread(30:130,135:235,1)));
% saveas(gcf, strcat(resultsDir, 'mandril_1'), 'png');

figure('visible', 'off')
% subplot(2,2,2)
imshow(uint8(bimage_spread(30:130,135:235,2)));
% saveas(gcf, strcat(resultsDir, 'mandril_2'), 'png');


figure('visible', 'off')
% subplot(2,2,3)
imshow(uint8(bimage_spread(30:130,135:235,5)));
% saveas(gcf, strcat(resultsDir, 'mandril_5'), 'png');

figure('visible', 'off')
% subplot(2,2,4)
imshow(uint8(bimage_spread(30:130,135:235,7)));
% saveas(gcf, strcat(resultsDir, 'mandril_7'), 'png');


% for i =1:7
%     figure(i)
% %     subplot(,3,i),
%     imshow(uint8(image_spread(:,:,i)));
% end

%%

