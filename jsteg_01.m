% Quantization is the process of reducing the number of bits needed to 
% store an integer value by reducing the precision of the integer. 
% Given a matrix of DCT coefficients, we can generally reduce the precision 
% of the coefficients more and more as we move away from the DC coefficient. 
% This is because the farther away we are from the DC coefficient, the less 
% the element contributes to the graphical image, and therefore, 
% the less we care about maintaining rigorous precision in its value (Nelson 364).
% 
% The JPEG algorithm implements quantization using a separate quantization matrix, an example of which is shown below.

clear
clc
close all

% Stanford Q
% Q= ([ 3,5,7,9,11,13,15,17;
%     5,7,9,11,13,15,17,19;
%     7,9,11,13,15,17,19,21;
%     9,11,13,15,17,19,21,23;
%     11,13,15,17,19,21,23,25;
%     13,15,17,19,21,23,25,27;
%     15,17,19,21,23,25,27,29;
%     17,19,21,23,25,27,29,31]);

% Standard Q
Q = [
     16  11  10  16  24   40   51   61;
     12  12  14  19  26   58   60   55;
     14  13  16  24  40   57   69   56;
     14  17  22  29  51   87   80   62;
     18  22  37  56  68   109  103  77;
     24  35  55  64  81   104  113  92;
     49  64  78  87  103  121  120  101;
     72  92  95  98  112  100  103  99
    ];


% Read input image
cover = imread('./cover/lena.png');
cover = rgb2gray(cover);

dct88 = zeros(size(cover));
dct88_recon = zeros(size(cover));
reconstructed = uint8(zeros(size(cover)));

%% Create DCT (8*8)
for i = 1:64
   
    for j = 1:64
        % Get the 8x8 block
        % fprintf('Finding a block in the rang of: %d to %d\n', (j-1)*8+1,j*8);
        block = cover((i-1)*8+1:i*8, (j-1)*8+1:j*8);
        
        % DCT 8x8 block
        dct_block = dct2(block);

        % Quantize
        dct_block_quant = round(dct_block./Q);
        
        % Dequantize
%         dct_block_dequant = dct_block_quant.*Q;

        % Concatentate into big DCT matrix
%         dct88((i-1)*8+1:i*8, (j-1)*8+1:j*8) = dct_block_dequant;
        dct88((i-1)*8+1:i*8, (j-1)*8+1:j*8) = dct_block_quant;
    end
    
end

%% Embed bits
message = 'hello world';
s = str2bitstream(message);

% s = '11111111111111111111111111111111111111111111111111111111';
% s = '00000000000000000000000000000000000000000000000000000000';
bits_to_embed = length(s);
bits_embedded = 0;
bit_counter = 1;
bit_package_size = 1;
encoded = false;

dct88_out = int16(dct88);

for i = 1: size(dct88_out,1)
    for j = 1: size(dct88_out,2)
        
        if(abs(dct88_out(i,j)) ~=1 && dct88_out(i,j) ~=0)        % not 0 or 1
%             if(~(mod(i-1,8) == 0 && mod(j-1,8) == 0))   % not a corner
                
                if(bits_embedded >= bits_to_embed )
                    encoded = true;
                    break ;
                end
                
                % Bit package to embed
                bit_package = s(bit_counter:bit_counter+(bit_package_size-1));
                bit_counter = bit_counter + bit_package_size;

                % Embed the bit package
                dct88_out(i,j) = embedBitPackage(bit_package, dct88_out(i,j));
                bits_embedded = bits_embedded + bit_package_size;
%             end
            
        end
        
    end
    
    if(encoded)
        break;
    end
end



%% Reconstruct the image

dct88_out = dct88;

for i = 1:64
   
    for j = 1:64
        % Get the 8x8 block
        dct_block = dct88_out((i-1)*8+1:i*8, (j-1)*8+1:j*8);
        
        % DCT 8x8 block
        block = idct2(dct_block);

        % Concatentate into reconstructd image
        reconstructed((i-1)*8+1:i*8, (j-1)*8+1:j*8) = block;
    end
    
end

%% Find DCT88 of reconstructed image

for i = 1:64
   
    for j = 1:64
        % Get the 8x8 block
        block = reconstructed((i-1)*8+1:i*8, (j-1)*8+1:j*8);
        
        % DCT 8x8 block
        dct_block = dct2(block);

        % Quantize
%         dct_block_quant = round(dct_block./Q);
        dct_block_quant = round(dct_block);
        
        % Dequantize
%         dct_block_dequant = dct_block_quant.*Q;

        % Concatentate into big DCT matrix
        dct88_recon((i-1)*8+1:i*8, (j-1)*8+1:j*8) = dct_block_quant;
    end
    
end

%% Decode reconstructed DCT88

bits_to_retrieve = bits_to_embed;
bits_retrieved = 0;
bit_counter = 1;
decoded = false;
z = '';


for i = 1: size(dct88_out,1)
    for j = 1: size(dct88_out,2)
        
        if(abs(dct88_recon(i,j)) ~=1 && dct88_recon(i,j) ~=0)        % not 0 or 1
%             if(~(mod(i-1,8) == 0 && mod(j-1,8) == 0))   % not a corner
                
                z = [z, decodeBitPackage(dct88_recon(i,j), bit_package_size)];
                bits_retrieved = bits_retrieved + bit_package_size;
                if(bits_retrieved >= bits_to_retrieve )
                    decoded = true;
                    break ;
                end
                
%             end
            
        end
        
    end
    
    if(decoded)
        break;
    end
end

decoded_message = '';
for i = 1:8:bits_to_embed
    decoded_message = [decoded_message, char(bin2dec(z(i:i+7)))];
end

fprintf('---- ORIGINAL MESSAGE: \n''%s'' \n\n', message);
fprintf('---- DECODED MESSAGE: \n''%s'' \n', decoded_message);


%% Check the results
% 
dct_whole_cover = dct2(cover);
dct_whole_recon = dct2(reconstructed);

figure()
subplot(2,2,1)
imshow(cover)
title('Original')
subplot(2,2,2)
imshow(reconstructed)
title('Reconstructed')
subplot(2,2,3)
imshow(log(abs(dct_whole_cover)), [])
colormap(gca, jet(64))
colorbar
subplot(2,2,4)
imshow(log(abs(dct_whole_recon)), [])
colormap(gca, jet(64))
colorbar
% figure()
% imshow(log(abs(cover_dct)), [])
% colormap(gca, jet(64))
% colorbar

