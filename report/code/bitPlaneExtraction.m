% -----------------------------------------------------------------------
% BIT PLANE EXTRACTION
% 
% Description:  Split an uint8 image into its 8 bit planes
%               Reconstruct the image
% Subject:      DSP - 2019
% Authors:      Matteo Kalogirou, Jake Pencharz
% -----------------------------------------------------------------------
close all
clc
clear

% ---- Split the image into bit planes
im = imread('./cover/lena_std.tiff');
im = rgb2gray(im);
im = double(im);

f1 = figure();
set(gcf, 'Color', 'None')
bitplane = zeros(8,512,512);

for i=0:7
    
    bitplane(i+1,:,:) = mod(floor(im/2^i), 2);
    subplot(2, 4, i+1)
    imshow(squeeze(bitplane(i+1,:,:)))
    title(strcat('Bitplane: ', num2str(i+1)))
    
end

% ---- Reconstruct the image using a subset of the bit planes
f2 = figure();
set(gcf, 'Color', 'None')
layer_count = 1;
combined = zeros(size(im));
for i = 8:-1:5
   
    disp(i)
    layer = squeeze(bitplane(i,:,:)) * 2^(i-1);
    combined = combined + layer;
    subplot(2, 2, i-4)
    imshow(uint8(combined))
    title(strcat(num2str(layer_count), ' MSBs'))
    layer_count = layer_count + 1;
    
end