clc
clear
close all

%% ===== Read in the data =====


resultsDir = './report/figures/';
fileID = fopen('./input/accents.txt', 'r');
message = fscanf(fileID, '%c');
%%
%Flowers

meanMSE_flower = [];
meanPSNR_flower = [];
meanSSI_flower = [];

for bpp =1:7

Flowers = struct;
Flowers.jpgPath = './input/Images/flower/';
Flowers.jpgFiles = dir(strcat(Flowers.jpgPath, '*jpg'));
Flowers.MSE = [];
Flowers.PSNR = [];
Flowers.SSI = [];

for file = Flowers.jpgFiles';
    
    filename = strcat(Flowers.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
    
    raw_image = imresize(raw_image, [512 512]);    
    steg = image2SpreadSteg(raw_image, message, bpp);
    
    %Calculate the MSE, PSNR, SSI
    Flowers.MSE = [Flowers.MSE mse(steg, raw_image)];    
    Flowers.PSNR = [Flowers.PSNR PSNR(steg, raw_image, 255)];    
    Flowers.SSI = [Flowers.SSI ssim(steg, raw_image)];        
end

meanMSE_flower = [meanMSE_flower mean(Flowers.MSE)];
meanPSNR_flower = [meanPSNR_flower mean(Flowers.PSNR)];
meanSSI_flower = [meanSSI_flower mean(Flowers.SSI)];

end


% CARS

meanMSE_car = [];
meanPSNR_car = [];
meanSSI_car = [];

for bpp =1:7
    Cars = struct;
    Cars.jpgPath = './input/Images/cars/';
    Cars.jpgFiles = dir(strcat(Cars.jpgPath, '*jpg'));
    Cars.MSE = [];
    Cars.PSNR = [];
    Cars.SSI = [];
    
    for file = Cars.jpgFiles';
    
        filename = strcat(Cars.jpgPath, file.name);  
        raw_image = rgb2gray(imread(filename));

        raw_image = imresize(raw_image, [512 512]);

        steg = image2SpreadSteg(raw_image, message, bpp);

        %Calculate the MSE, PSNR, SSI
        Cars.MSE = [Cars.MSE mse(steg, raw_image)];
        Cars.PSNR = [Cars.PSNR PSNR(steg, raw_image, 255)];
        Cars.SSI = [Cars.SSI ssim(steg, raw_image)];        
    end
meanMSE_car = [meanMSE_car mean(Cars.MSE)];
meanPSNR_car = [meanPSNR_car mean(Cars.PSNR)];
meanSSI_car = [meanSSI_car mean(Cars.SSI)];
    
end

% OBJECTS

meanMSE_objects = [];
meanPSNR_objects = [];
meanSSI_objects = [];

for bpp=1:7
    Objects = struct;
    Objects.jpgPath = './input/Images/objects/';
    Objects.jpgFiles = dir(strcat(Objects.jpgPath, '*JPG'));
    Objects.MSE = [];
    Objects.PSNR = [];
    Objects.SSI = [];
    
    for file = Objects.jpgFiles';
    
        filename = strcat(Objects.jpgPath, file.name);  
        raw_image = rgb2gray(imread(filename));

        raw_image = imresize(raw_image, [512 512]);

        steg = image2SpreadSteg(raw_image, message, bpp);

        %Calculate the MSE, PSNR, SSI
        Objects.MSE = [Objects.MSE mse(steg, raw_image)];
        Objects.PSNR = [Objects.PSNR PSNR(steg, raw_image, 255)];
        Objects.SSI = [Objects.SSI ssim(steg, raw_image)];
        
    end
meanMSE_objects = [meanMSE_objects mean(Objects.MSE)];
meanPSNR_objects = [meanPSNR_objects mean(Objects.PSNR)];
meanSSI_objects = [meanSSI_objects mean(Objects.SSI)];
    
end

% PEOPLE

meanMSE_people = [];
meanPSNR_people = [];
meanSSI_people = [];

for bpp=1:7
    
    People = struct;
    People.jpgPath = './input/Images/faces/';
    People.jpgFiles = dir(strcat(People.jpgPath, '*jpg'));
    People.MSE = [];
    People.PSNR = [];
    People.SSI = [];
    
    for file = People.jpgFiles';    
        filename = strcat(People.jpgPath, file.name);  
        raw_image = rgb2gray(imread(filename));

        raw_image = imresize(raw_image, [512 512]);

        steg = image2SpreadSteg(raw_image, message, bpp);

        %Calculate the MSE, PSNR, SSI
        People.MSE = [People.MSE mse(steg, raw_image)];
        People.PSNR = [People.PSNR PSNR(steg, raw_image, 255)];
        People.SSI = [People.SSI ssim(steg, raw_image)];        
    end
meanMSE_people = [ meanMSE_people mean(People.MSE)];
meanPSNR_people = [meanPSNR_people mean(People.PSNR)];
meanSSI_people = [meanSSI_people mean(People.SSI)];

end


%% Plotting - MSE, PSNR and SSI with means

figure();
subplot(1,3,1),
% title('Averaged Mean Squared Error as a function of embedded bits per pixel.');
hold on;
plot(meanMSE_flower, '-xr', 'LineWidth', 1);
plot(meanMSE_car, '-dc', 'LineWidth', 1);
plot(meanMSE_objects, '-ob', 'LineWidth', 1);
plot(meanMSE_people, '-sg', 'LineWidth', 1);
plot(bimage_spread_MSE, '-sk', 'LineWidth', 1); 
plot(gimage_spread_MSE, '-sm', 'LineWidth', 1);
hold off;
set(gca,'xtick',1:7, 'FontSize',14); 
xlabel('Bits Per Pixel');
ylabel('MSE');
leg = legend('Flowers', 'Cars', 'Objects', 'People', 'B&W', 'Gray');
leg.FontSize = 16;
leg.Location = 'NorthWest';

% saveas(gcf, strcat(resultsDir,'meanMSEPerBPP.png'), 'png');
%
% figure();
% title('Peak Signal to Noise Ratio as a function of embedding bits per pixel');
subplot(1,3,2),
hold on;
plot(meanPSNR_flower, '-xr', 'LineWidth', 1);
plot(meanPSNR_car, '-dc', 'LineWidth', 1);
plot(meanPSNR_objects, '-ob', 'LineWidth', 1);
plot(meanPSNR_people, '-sg', 'LineWidth', 1);
plot(bimage_spread_PSNR, '-sk', 'LineWidth', 1); 
plot(gimage_spread_PSNR, '-sm', 'LineWidth', 1);
hold off;
set(gca,'xtick',1:7, 'FontSize',14);  
xlabel('Bits per pixel');
ylabel('PSNR (dB)');
leg = legend('Flowers', 'Cars', 'Objects', 'People', 'B&W', 'Gray');
leg.FontSize = 16;

% saveas(gcf, strcat(resultsDir,'meanPSNRPerBPP.png'), 'png');

%

% figure();
% title('Structural Similarity Index as a function of embedding bits per pixel')
subplot(1,3,3),
hold on;
plot(meanSSI_flower, '-xr', 'LineWidth', 1);
plot(meanSSI_car, '-dc', 'LineWidth', 1);
plot(meanSSI_objects, '-ob', 'LineWidth', 1);
plot(meanSSI_people, '-sg', 'LineWidth', 1);
plot(bimage_spread_SSI, '-sk', 'LineWidth', 1); 
plot(gimage_spread_SSI, '-sm', 'LineWidth', 1);
hold off;
set(gca,'xtick',1:7, 'FontSize',14); 
xlabel('Bits per pixel');
ylabel('SSI');
leg = legend('Flowers', 'Cars', 'Objects', 'People','B&W', 'Gray');
leg.FontSize = 16;
leg.Location = 'SouthWest';

% saveas(gcf, strcat(resultsDir,'meanSSIPerBPP.png'), 'png');
% saveas(gcf, strcat(resultsDir,'meanPerBPP.png'), 'png');


