%% IMAGE STEGANOGRAPHY - EXAMPLE 01

% Description:  Embedding some text within an image using simple LSB
%               substitution technique.
% Subject:      DSP - 2019
% Authors:      Matteo Kalogirou, Jake Pencharz

%% Program Preamble

clc
clear 
close all

% Choose parameters
bit_package_size = 3;

% ---- Read in the image
input = imread('./cover/lena.png');
input = rgb2gray(input);

% ---- Read in a long message to be hidden
fileID = fopen('./input/accents.txt', 'r');
message = fscanf(fileID, '%c');

steg = image2steg(input, message, bit_package_size);

%% Now decode

decoded_message = decodeSteg(steg, bit_package_size); 

% fprintf('---- ORIGINAL MESSAGE: \n''%s'' \n\n', message);
% fprintf('---- DECODED MESSAGE: \n''%s'' \n', decoded_message);


%% Plot
plot = true;
difference = abs((input-steg));

if(plot)
    figure()
    subplot(1,3,1)
    imshow(input)
    title('Original')

    subplot(1,3,2)
    imshow(steg)
    title('Steg')

    subplot(1,3,3)
    imshow(difference*50/bit_package_size)
    title('Amplified Difference')
end

