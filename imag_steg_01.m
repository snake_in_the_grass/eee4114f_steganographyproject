%% IMAGE STEGANOGRAPHY - EXAMPLE 01

% Description:  Embedding some text within an image using simple LSB
%               substitution technique.
% Subject:      DSP - 2019
% Authors:      Matteo Kalogirou, Jake Pencharz

%% Program Preamble

clc
clear 
close all

% Choose parameters
test = false;
plot = true;
bit_package_size = 2;

% ---- Read in the image
input = imread('./cover/stegosaurus.jpg');
input = rgb2gray(input);



if(test)
    input = uint8(magic(10));
end

% ---- Process the message to be hidden
fileID = fopen('./input/accents.txt', 'r');
message = fscanf(fileID, '%c');
if(test)
    message = 'hello there';
end
fprintf('Number of bits to be emedded: %d\n', length(message)*8);
s = str2bitstream(message);

output = input;
width = size(output,1);
height = size(output,2);
bits_to_embed = length(message)*8;

% Check the size
if(bits_to_embed > width * height)
    disp('Cannot embed all the bits!');
    return;
end

bits_embedded = 0;
bit_counter = 1;
encoded = false;

for i = 1:width

    for j = 1:height
        
        if(bits_embedded >= bits_to_embed )
            encoded = true;
            break ;
        end
       
        % Bit package to embed
        bit_package = s(bit_counter:bit_counter+(bit_package_size-1));
        bit_counter = bit_counter + bit_package_size;
        
        % Embed the bit package
        output(i,j) = embedBitPackage(bit_package, output(i,j));
        bits_embedded = bits_embedded + bit_package_size;
        
    end
    
    if(encoded)
        break;
    end

end

imwrite(output, './output/out_steg01.png');

difference = abs((input-output));

if(plot)
    figure()
    subplot(1,3,1)
    imshow(input)
    title('Original')

    subplot(1,3,2)
    imshow(output)
    title('Steg')

    subplot(1,3,3)
    imshow(difference*50/bit_package_size)
    title('Amplified Difference')
end


%% Now decode

z = '';
bits_retrieved = 0;
decoded = false;

for i = 1:width
    
    for j = 1:height
        z = [z, decodeBitPackage(output(i,j), bit_package_size)];
        bits_retrieved = bits_retrieved + bit_package_size;
        if(bits_retrieved >= bits_to_embed)
            decoded = true;
            break;
        end
        
    end
    if(decoded)
        break;
    end
end

decoded_message = '';
for i = 1:8:bits_to_embed
    decoded_message = [decoded_message, char(bin2dec(z(i:i+7)))];
end

fprintf('---- ORIGINAL MESSAGE: \n''%s'' \n\n', message);
fprintf('---- DECODED MESSAGE: \n''%s'' \n', decoded_message);
