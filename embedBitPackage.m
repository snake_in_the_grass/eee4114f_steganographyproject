function [ y ] = embedBitPackage( bit_package, x ) 
%   bit_package = a char array of bits
%   x = a decimal value in which the bits will be embedded
%   y = the modified x (with bits already embedded)
    
    no_bits = length(bit_package);
    
    
    if (isa(x, 'int16'))    % FOR AUDIO
        % maximum number for a 16 bit int
        mask16 = int16(32767);  
        
        % modify the mask to put zeros where we are going to insert
        for i = 0:(no_bits-1)
           mask16 = mask16 - 2^i;
        end
       
        % Check if the number is negative
        if(x < 0)
            x = x*-1;
            y = bitand(x, mask16);
            y = bitor(y, bin2dec(bit_package));
            y = y*-1;
        else
            y = bitand(x, mask16);
            y = bitor(y, bin2dec(bit_package));
        end
        
    elseif(isa(x, 'uint8'))
        
        mask8 = uint8(255);
        
        % modify the mask to put zeros where we are going to insert
        for i = 0:(no_bits-1)
           mask8 = mask8 - 2^i;
        end
        
        y = bitand(x, mask8);
        y = bitor(y, bin2dec(bit_package));

%         temp = dec2bin(x,8);
% 
%         for i = 0: no_bits-1
%             temp(end-i) = bit_package(end - i);
%         end
% 
%         y = bin2dec(temp);
    end

end

