
clc
clear
close all

%% ===== Read in the data =====

%Cars, Flowers, Objects
%file dir, num Images, mse, psnr, ssi.

resultsDir = './report/figures/';
fileID = fopen('./input/accents.txt', 'r');
message = fscanf(fileID, '%c');
bpp = 1;

Flowers = struct;
Flowers.jpgPath = './input/Images/flower/';
Flowers.jpgFiles = dir(strcat(Flowers.jpgPath, '*jpg'));
Flowers.MSE = [];
Flowers.PSNR = [];
Flowers.SSI = [];
%%
Cars = struct;
Cars.jpgPath = './input/Images/cars/';
Cars.jpgFiles = dir(strcat(Cars.jpgPath, '*jpg'));
Cars.MSE = [];
Cars.PSNR = [];
Cars.SSI = [];

Objects = struct;
Objects.jpgPath = './input/Images/objects/';
Objects.jpgFiles = dir(strcat(Objects.jpgPath, '*JPG'));
Objects.MSE = [];
Objects.PSNR = [];
Objects.SSI = [];

People = struct;
People.jpgPath = './input/Images/faces/';
People.jpgFiles = dir(strcat(People.jpgPath, '*jpg'));
People.MSE = [];
People.PSNR = [];
People.SSI = [];

% Nature = struct;
% Nature.jpgPath = './Images/nature/';
% Nature.jpgFiles = dir(strcat(Nature.jpgPath, '*jpg'));
% Nature.MSE = [];
% Nature.PSNR = [];
% Nature.SSI = [];
%%

for file = Flowers.jpgFiles';
    
    filename = strcat(Flowers.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
    
    raw_image = imresize(raw_image, [512 512]);    
    steg = image2steg(raw_image, message, bpp);
    
    %Calculate the MSE, PSNR, SSI
    Flowers.MSE = [Flowers.MSE mse(steg, raw_image)];    
    Flowers.PSNR = [Flowers.PSNR PSNR(steg, raw_image, 255)];    
    Flowers.SSI = [Flowers.SSI ssim(steg, raw_image)];        
end
Flowers.MSE_avg = mean(Flowers.MSE);
Flowers.PSNR_avg = mean(Flowers.PSNR);
Flowers.SSI_avg = mean(Flowers.SSI);
%%
for file = Cars.jpgFiles';
    
    filename = strcat(Cars.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
        
    raw_image = imresize(raw_image, [512 512]);
    
    steg = image2steg(raw_image, message, bpp);
        
    %Calculate the MSE, PSNR, SSI
    Cars.MSE = [Cars.MSE mse(steg, raw_image)];
    Cars.PSNR = [Cars.PSNR PSNR(steg, raw_image, 255)];
    Cars.SSI = [Cars.SSI ssim(steg, raw_image)];        
end
Cars.MSE_avg = mean(Cars.MSE);
Cars.PSNR_avg = mean(Cars.PSNR);
Cars.SSI_avg = mean(Cars.SSI);
%%
for file = Objects.jpgFiles';
    
    filename = strcat(Objects.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
    
    raw_image = imresize(raw_image, [512 512]);
    
    steg = image2steg(raw_image, message, bpp);
        
    %Calculate the MSE, PSNR, SSI
    Objects.MSE = [Objects.MSE mse(steg, raw_image)];
    Objects.PSNR = [Objects.PSNR PSNR(steg, raw_image, 255)];
    Objects.SSI = [Objects.SSI ssim(steg, raw_image)];
        
end
Objects.MSE_avg = mean(Objects.MSE);
Objects.PSNR_avg = mean(Objects.PSNR);
Objects.SSI_avg = mean(Objects.SSI);

%%
% for file = Nature.jpgFiles';
%     
%     filename = strcat(Nature.jpgPath, file.name);  
%     raw_image = rgb2gray(imread(filename));       
%     raw_image = imresize(raw_image, [512 512]);
%
%     steg = image2steg(raw_image, message, bpp);
%     steg = image2steg(raw_image, message, bpp);
%     
%     decoded_message = decodeSpreadSteg(steg, bpp);
%     
%     %Calculate the MSE, PSNR, SSI
%     Nature.MSE = [Nature.MSE mse(steg, raw_image)];
%     Nature.PSNR = [Nature.PSNR PSNR(steg, raw_image, 255)];
%     Nature.SSI = [Nature.SSI ssim(steg, raw_image)];     
% end
% Nature.MSE_avg = mean(Nature.MSE);
% Nature.PSNR_avg = mean(Nature.PSNR);
% Nature.SSI_avg = mean(Nature.SSI);
%%
for file = People.jpgFiles';
    
    filename = strcat(People.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
     
    raw_image = imresize(raw_image, [512 512]);
    
    steg = image2steg(raw_image, message, bpp);
        
    %Calculate the MSE, PSNR, SSI
    People.MSE = [People.MSE mse(steg, raw_image)];
    People.PSNR = [People.PSNR PSNR(steg, raw_image, 255)];
    People.SSI = [People.SSI ssim(steg, raw_image)];        
end
People.MSE_avg = mean(People.MSE);
People.PSNR_avg = mean(People.PSNR);
People.SSI_avg = mean(People.SSI);

%% Plotting - MSE, PSNR and SSI with means

figure();
title(strcat('Mean Squared Error - ',num2str(bpp), 'Bit per pixel'));
hold on;
plot(Flowers.MSE, 'xr', 'LineWidth', 2);
plot(Cars.MSE, 'dc', 'LineWidth', 2);
plot(Objects.MSE, 'ob', 'LineWidth', 2);
plot(People.MSE, '+g', 'LineWidth', 2);
hold off;
xlabel('Samples');
ylabel('Mean Squared Error');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 12;
ylabel('MSE');

saveas(gcf, strcat(resultsDir, 'mse_contig_',num2str(bpp),'bpp.png'), 'png');

%%
figure();
title(strcat('Peak Signal to Noise Ratio- ',num2str(bpp), 'Bit per pixel'));
hold on;
plot(Flowers.PSNR, 'xr', 'LineWidth', 2);
plot(Cars.PSNR, 'dc', 'LineWidth', 2);
plot(Objects.PSNR, 'ob', 'LineWidth', 2);
plot(People.PSNR, '+g', 'LineWidth', 2);
hold off;
xlabel('Samples');
ylabel('Peak Signal to Noise Ratio');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 12;
ylabel('PSNR');
saveas(gcf, strcat(resultsDir, 'psnr_contig_',num2str(bpp),'bpp.png'), 'png');

%%
figure();
title(strcat('Structural Similarity Index - ',num2str(bpp), 'Bit per pixel'));
hold on;
plot(Flowers.SSI, 'xr', 'LineWidth', 2);
plot(Cars.SSI, 'dc', 'LineWidth', 2);
plot(Objects.SSI, 'ob', 'LineWidth', 2);
plot(People.SSI, '+g', 'LineWidth', 2);
hold off;
xlabel('Samples');
ylabel('Structural similarity index');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 12;
ylabel('SSI');
saveas(gcf, strcat(resultsDir, 'ssi_contig_',num2str(bpp),'bpp.png'), 'png');








