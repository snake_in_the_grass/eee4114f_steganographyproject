function [ bit_package ] = decodeBitPackage( x, package_size )

    % bit_package: a string of binary
    
    
    x = dec2bin(abs(x), 8);
    bit_package = x(end-(package_size-1):end);

end

