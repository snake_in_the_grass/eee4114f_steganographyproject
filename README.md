# DSP Project

## Directory Structure 

All `.m` files are in the root directory.

- __input__ : any input text file

- __output__ : any output steg images or audio

- __cover__ : any cover images (to hide stuff inside)

- __report__ : self explanatory

    - __figures__ : all report figures

    - __code__ : any code samples to be used in the report


