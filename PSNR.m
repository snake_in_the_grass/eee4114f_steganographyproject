function [ y ] = PSNR( A, B, MPV)
% Returns the Peak signal to noise ratio between images A and B according to:
%   PSNR = 10log(MPV^2/MSE)
% The maximum pixel value MPV = 2^8-1 = 255

y = 10*log10(MPV^2/mse(A,B));


end

