
clc
clear
close all

%% ===== Read in the data =====

%Cars, Flowers, Objects
%file dir, num Images, mse, psnr, ssi.

resultsDir = './report/figures/';
fileID = fopen('./input/accents.txt', 'r');
message = fscanf(fileID, '%c');
bpp = 2;

Flowers = struct;
Flowers.jpgPath = './input/Images/flower/';
Flowers.jpgFiles = dir(strcat(Flowers.jpgPath, '*jpg'));
Flowers.MSE = [];
Flowers.PSNR = [];
Flowers.SSI = [];

Cars = struct;
Cars.jpgPath = './input/Images/cars/';
Cars.jpgFiles = dir(strcat(Cars.jpgPath, '*jpg'));
Cars.MSE = [];
Cars.PSNR = [];
Cars.SSI = [];

Objects = struct;
Objects.jpgPath = './input/Images/objects/';
Objects.jpgFiles = dir(strcat(Objects.jpgPath, '*JPG'));
Objects.MSE = [];
Objects.PSNR = [];
Objects.SSI = [];

People = struct;
People.jpgPath = './input/Images/faces/';
People.jpgFiles = dir(strcat(People.jpgPath, '*jpg'));
People.MSE = [];
People.PSNR = [];
People.SSI = [];

%%

for file = Flowers.jpgFiles';
    
    filename = strcat(Flowers.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
    
    raw_image = imresize(raw_image, [512 512]);    
    steg = image2SpreadSteg(raw_image, message, bpp);
    
    %Calculate the MSE, PSNR, SSI
    Flowers.MSE = [Flowers.MSE mse(steg, raw_image)];    
    Flowers.PSNR = [Flowers.PSNR PSNR(steg, raw_image, 255)];    
    Flowers.SSI = [Flowers.SSI ssim(steg, raw_image)];        
end
Flowers.MSE_avg = mean(Flowers.MSE);
Flowers.PSNR_avg = mean(Flowers.PSNR);
Flowers.SSI_avg = mean(Flowers.SSI);

for file = Cars.jpgFiles';
    
    filename = strcat(Cars.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
        
    raw_image = imresize(raw_image, [512 512]);
    
    steg = image2SpreadSteg(raw_image, message, bpp);
        
    %Calculate the MSE, PSNR, SSI
    Cars.MSE = [Cars.MSE mse(steg, raw_image)];
    Cars.PSNR = [Cars.PSNR PSNR(steg, raw_image, 255)];
    Cars.SSI = [Cars.SSI ssim(steg, raw_image)];        
end
Cars.MSE_avg = mean(Cars.MSE);
Cars.PSNR_avg = mean(Cars.PSNR);
Cars.SSI_avg = mean(Cars.SSI);

for file = Objects.jpgFiles';
    
    filename = strcat(Objects.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
    
    raw_image = imresize(raw_image, [512 512]);
    
    steg = image2SpreadSteg(raw_image, message, bpp);
        
    %Calculate the MSE, PSNR, SSI
    Objects.MSE = [Objects.MSE mse(steg, raw_image)];
    Objects.PSNR = [Objects.PSNR PSNR(steg, raw_image, 255)];
    Objects.SSI = [Objects.SSI ssim(steg, raw_image)];
        
end
Objects.MSE_avg = mean(Objects.MSE);
Objects.PSNR_avg = mean(Objects.PSNR);
Objects.SSI_avg = mean(Objects.SSI);


for file = People.jpgFiles';
    
    filename = strcat(People.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
     
    raw_image = imresize(raw_image, [512 512]);
    
    steg = image2SpreadSteg(raw_image, message, bpp);
        
    %Calculate the MSE, PSNR, SSI
    People.MSE = [People.MSE mse(steg, raw_image)];
    People.PSNR = [People.PSNR PSNR(steg, raw_image, 255)];
    People.SSI = [People.SSI ssim(steg, raw_image)];        
end
People.MSE_avg = mean(People.MSE);
People.PSNR_avg = mean(People.PSNR);
People.SSI_avg = mean(People.SSI);

%% Getting PDF - MSE, PSNR, SSI

x_val = 0.015:0.0001:0.035;
    mse_pd_flowers = fitdist(Flowers.MSE','Normal');
    mse_pdf_flowers = pdf(mse_pd_flowers, x_val);
    mse_pd_people = fitdist(People.MSE','Normal');
    mse_pdf_people = pdf(mse_pd_people, x_val);
    mse_pd_cars = fitdist(Cars.MSE','Normal');
    mse_pdf_cars = pdf(mse_pd_cars, x_val);
    mse_pd_objects = fitdist(Objects.MSE','Normal');
    mse_pdf_objects = pdf(mse_pd_objects, x_val);

figure();
hold on;
% title('MSE PDF');
plot(x_val, mse_pdf_flowers,'LineWidth', 2);
plot(x_val, mse_pdf_cars,'LineWidth', 2); 
plot(x_val, mse_pdf_objects,'LineWidth', 2);
plot(x_val, mse_pdf_people,'LineWidth', 2);
axis([0.015 0.035 0 900]);
set(gca, 'FontSize', 16);
xlabel('MSE'); ylabel('Normal Distribution');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 18;
hold off;
% saveas(gcf, strcat(resultsDir, 'mse_pdf_',num2str(bpp),'bpp.png'), 'png');

    
%%
x_val = 63:0.01:65.5;
    psnr_pd_flowers = fitdist(Flowers.PSNR','Normal');
    psnr_pdf_flowers = pdf(psnr_pd_flowers, x_val);
    psnr_pd_people = fitdist(People.PSNR','Normal');
    psnr_pdf_people = pdf(psnr_pd_people, x_val);
    psnr_pd_cars = fitdist(Cars.PSNR','Normal');
    psnr_pdf_cars = pdf(psnr_pd_cars, x_val);
    psnr_pd_objects = fitdist(Objects.PSNR','Normal');
    psnr_pdf_objects = pdf(psnr_pd_objects, x_val);

figure();
hold on;
% title('PSNR PDF');
plot(x_val, psnr_pdf_flowers,'LineWidth', 2);
plot(x_val, psnr_pdf_cars,'LineWidth', 2); 
plot(x_val, psnr_pdf_objects,'LineWidth', 2);
plot(x_val, psnr_pdf_people,'LineWidth', 2);
set(gca, 'FontSize', 16);
xlabel('PSNR'); ylabel('Normal Distribution');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 18;
hold off;
% saveas(gcf, strcat(resultsDir, 'psnr_pdf_',num2str(bpp),'bpp.png'), 'png');

%%
x_val = 0.998:0.00001:1.001;
    ssi_pd_flowers = fitdist(Flowers.SSI','Normal');
    ssi_pdf_flowers = pdf(ssi_pd_flowers, x_val);
    ssi_pd_people = fitdist(People.SSI','Normal');
    ssi_pdf_people = pdf(ssi_pd_people, x_val);
    ssi_pd_cars = fitdist(Cars.SSI','Normal');
    ssi_pdf_cars = pdf(ssi_pd_cars, x_val);
    ssi_pd_objects = fitdist(Objects.SSI','Normal');
    ssi_pdf_objects = pdf(ssi_pd_objects, x_val);

figure();
hold on;
% title('SSI PDF');
plot(x_val, ssi_pdf_flowers,'LineWidth', 2);
plot(x_val, ssi_pdf_cars,'LineWidth', 2); 
plot(x_val, ssi_pdf_objects,'LineWidth', 2);
plot(x_val, ssi_pdf_people,'LineWidth', 2);
set(gca, 'FontSize', 16);
xlabel('SSI'); ylabel('Normal Distribution');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 18;
hold off;
% saveas(gcf, strcat(resultsDir, 'ssi_pdf_',num2str(bpp),'bpp.png'), 'png');    

%% Plotting - MSE, PSNR and SSI

figure();
% title(strcat('Mean Squared Error - ',num2str(bpp), 'Bit per pixel'));
hold on;
plot(Flowers.MSE, 'xr', 'LineWidth', 2);
plot(Cars.MSE, 'dc', 'LineWidth', 2);
plot(Objects.MSE, 'ob', 'LineWidth', 2);
plot(People.MSE, '+g', 'LineWidth', 2);
hold off;
set(gca, 'FontSize', 16);
xlabel('Samples'); ylabel('MSE');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 18;


saveas(gcf, strcat(resultsDir, 'mse_spread_',num2str(bpp),'bpp.png'), 'png');


%%
figure();
% title(strcat('Peak Signal to Noise Ratio- ',num2str(bpp), 'Bit per pixel'));
hold on;
plot(Flowers.PSNR, 'xr', 'LineWidth', 2);
plot(Cars.PSNR, 'dc', 'LineWidth', 2);
plot(Objects.PSNR, 'ob', 'LineWidth', 2);
plot(People.PSNR, '+g', 'LineWidth', 2);
hold off;
set(gca, 'FontSize', 16);
xlabel('Samples');
ylabel('PSNR');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 18;

saveas(gcf, strcat(resultsDir, 'psnr_spread_',num2str(bpp),'bpp.png'), 'png');

%%
figure();
% title(strcat('Structural Similarity Index - ',num2str(bpp), 'Bit per pixel'));
hold on;
plot(Flowers.SSI, 'xr', 'LineWidth', 2);
plot(Cars.SSI, 'dc', 'LineWidth', 2);
plot(Objects.SSI, 'ob', 'LineWidth', 2);
plot(People.SSI, '+g', 'LineWidth', 2);
hold off;
set(gca, 'FontSize', 16);
xlabel('Samples');
ylabel('SSI');
leg = legend('Flowers', 'Cars', 'Objects', 'People');
leg.FontSize = 18;

saveas(gcf, strcat(resultsDir, 'ssi_spread_',num2str(bpp),'bpp.png'), 'png');







