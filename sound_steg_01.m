%% IMAGE STEGANOGRAPHY - EXAMPLE 01

% Description:  Embedding some text within a sound clip using simple LSB
%               substitution technique.
% Subject:      DSP - 2019
% Authors:      Matteo Kalogirou, Jake Pencharz

%% Program Preamble

clc
clear 
close all

% Choose parameters
test = false;
plot = true;
bit_package_size = 3;

% ---- Read in the sound clip (Extract 5 seconds of mono sound)
Fs = 44100;
start_time = 9*Fs;
end_time = (9 + 5)*Fs;
[cover, Fs] = audioread('./cover/Meet Me In The City.flac',[start_time, end_time], 'native');
cover = cover(:,1);

original = audioplayer(cover, Fs);
% play(player);

% ---- Process the message to be hidden
fileID = fopen('./input/accents.txt', 'r');
message = fscanf(fileID, '%c');
if(test)
    message = 'hello there';
end
fprintf('Number of bits to be emedded: %d\n', length(message)*8);
s = str2bitstream(message);

output = cover;
cover_size = size(output,1);
bits_to_embed = length(message)*8;

% Check the size
if(bits_to_embed > cover_size )
    disp('Cannot embed all the bits!');
    return;
end



bits_embedded = 0;
bit_counter = 1;
encoded = false;


for i = 1:cover_size
   
    if(bits_embedded >= bits_to_embed )
        encoded = true;
        break ;
    end

    % Bit package to embed
    bit_package = s(bit_counter:bit_counter+(bit_package_size-1));
    bit_counter = bit_counter + bit_package_size;

    % Embed the bit package
%     fprintf('Output at position %d before: %d\n', i, output(i));
%     fprintf('Inserting bit package: ''%s''\n', bit_package);
    output(i) = embedBitPackage(bit_package, cover(i));
%     fprintf('Output at position %d after: %d\n', i, output(i));
    bits_embedded = bits_embedded + bit_package_size;
    
end

difference = abs((cover-output));

steg = audioplayer(output, Fs);
% play(steg);

