% -----------------------------------------------------------------------
% JSTEG Embedding (not compression)
% 
% Description:  Perform the embedding steps of the JPEG-JSTEG algorithm
% Subject:      DSP - 2019
% Authors:      Matteo Kalogirou, Jake Pencharz
% -----------------------------------------------------------------------
clear
clc
close all

% Standard Q
Q = [
     16  11  10  16  24   40   51   61;
     12  12  14  19  26   58   60   55;
     14  13  16  24  40   57   69   56;
     14  17  22  29  51   87   80   62;
     18  22  37  56  68   109  103  77;
     24  35  55  64  81   104  113  92;
     49  64  78  87  103  121  120  101;
     72  92  95  98  112  100  103  99
    ];

% Read input image
cover = imread('./cover/lena.png');
cover = rgb2gray(cover);

dct88 = zeros(size(cover));
dct88_recon = zeros(size(cover));
reconstructed = uint8(zeros(size(cover)));

%% Create DCT (8*8)
for i = 1:64
   
    for j = 1:64
        % Get the 8x8 block
        block = cover((i-1)*8+1:i*8, (j-1)*8+1:j*8);
        % DCT 8x8 block
        dct_block = dct2(block);
        % Quantize
        dct_block_quant = round(dct_block./Q); 
        dct88((i-1)*8+1:i*8, (j-1)*8+1:j*8) = dct_block_quant;
    end    
end

%% Embed bits
message = 'hello world';
s = str2bitstream(message);

bits_to_embed = length(s);
bits_embedded = 0;
bit_counter = 1;
bit_package_size = 1;
encoded = false;
dct88_out = int16(dct88);

for i = 1: size(dct88_out,1)
    for j = 1: size(dct88_out,2)
        
        if(abs(dct88_out(i,j)) ~=1 && dct88_out(i,j) ~=0)        % not 0 or 1                
                if(bits_embedded >= bits_to_embed )
                    encoded = true;
                    break ;
                end
                
                % Bit package to embed
                bit_package = s(bit_counter:bit_counter+(bit_package_size-1));
                bit_counter = bit_counter + bit_package_size;

                % Embed the bit package
                dct88_out(i,j) = embedBitPackage(bit_package, dct88_out(i,j));
                bits_embedded = bits_embedded + bit_package_size;            
        end
    end
    if(encoded)
        break;
    end
end