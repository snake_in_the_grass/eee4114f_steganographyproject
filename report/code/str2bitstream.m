function [ bit_stream ] = str2bitstream( message )
    bin_message = dec2bin(uint8(message),8);    % convert ascii numbers to bytes (characters array)

    N = length(bin_message);
    bit_stream = '';
    for i = 1:N
        bit_stream = [bit_stream, bin_message(i,:)];
    end
end

