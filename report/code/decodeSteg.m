% ------------------------------------------------------------------------_
% PARAMETERS:
% - steg:           uint8, rgb image matrix with embedded data
% - bitPackageSize: number of bits per pixel embedded
% 
% RETURNS:
% - message:        message embedded within steg
% ------------------------------------------------------------------------_


function [ message ] = decodeSteg( steg, bit_package_size )

    % Get size of the message (first 16 bits)
    z = '';
    eos = 16;
    for j = 1:eos
        z = [z, decodeBitPackage(steg(1,j), 1)];
    end

    bits_to_retrieve = bin2dec(z);    
    z = '';

    [rows, cols] = size(steg);
    bits_retrieved = 0;
    decoded = false;
    
    % Extract the raw bit stream
    for j = (eos+1):cols
        z = [z, decodeBitPackage(steg(1,j), bit_package_size)];
        bits_retrieved = bits_retrieved + bit_package_size;
        
        if(bits_retrieved == bits_to_retrieve)
                decoded = true;
                break;
        end
    end
        
    for i = 2:rows
        for j = 1:cols
            
            if(bits_retrieved == bits_to_retrieve)
                decoded = true;
                break;
            end
            
            z = [z, decodeBitPackage(steg(i,j), bit_package_size)];
            bits_retrieved = bits_retrieved + bit_package_size;
            
        end
        if(decoded)
            break;
        end
    end
    
    % Convert the bit stream to a string
    message = '';
    
    for i = 1:8:length(z)
        message = [message, char(bin2dec(z(i:i+7)))];
    end    
   
end

