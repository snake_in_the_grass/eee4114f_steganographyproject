
clc
clear
close all

%% ===== Read in the data =====

%Cars, Flowers, Objects
%file dir, num Images, mse, psnr, ssi.

fileID = fopen('./accents.txt', 'r');
message = fscanf(fileID, '%c');
bpp = 1;

Flowers = struct;
Flowers.jpgPath = './Images/flower/';
Flowers.jpgFiles = dir(strcat(Flowers.jpgPath, '*jpg'));
Flowers.MSE = [];
Flowers.PSNR = [];
Flowers.SSI = [];
%%
Cars = struct;
Cars.jpgPath = './Images/cars/';
Cars.jpgFiles = dir(strcat(Cars.jpgPath, '*jpg'));
Cars.MSE = [];
Cars.PSNR = [];
Cars.SSI = [];

Objects = struct;
Objects.jpgPath = './Images/objects/';
Objects.jpgFiles = dir(strcat(Objects.jpgPath, '*JPG'));
Objects.MSE = [];
Objects.PSNR = [];
Objects.SSI = [];

People = struct;
People.jpgPath = './Images/faces/';
People.jpgFiles = dir(strcat(People.jpgPath, '*gif'));
People.MSE = [];
People.PSNR = [];
People.SSI = [];

% Nature = struct;
% Nature.jpgPath = './Images/nature/';
% Nature.jpgFiles = dir(strcat(Nature.jpgPath, '*jpg'));
% Nature.MSE = [];
% Nature.PSNR = [];
% Nature.SSI = [];
%%

for file = Flowers.jpgFiles';
    
    filename = strcat(Flowers.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
    
    
    %Crop the image to by multiple of 8
    [h, w] = size(raw_image);
    w = floor(w/8)*8;
    h = floor(h/8)*8;
    raw_image = raw_image(1:h, 1:w);
    
    steg = image2steg(raw_image, message, bpp);
    
    decoded_message = decodeSteg(steg, bpp);
    
    %Calculate the MSE, PSNR, SSI
    Flowers.MSE = [Flowers.MSE mse(steg, raw_image)];    
    Flowers.PSNR = [Flowers.PSNR PSNR(steg, raw_image, 255)];    
    Flowers.SSI = [Flowers.SSI ssim(steg, raw_image)];        
end
Flowers.MSE_avg = mean(Flowers.MSE);
Flowers.PSNR_avg = mean(Flowers.PSNR);
Flowers.SSI_avg = mean(Flowers.SSI);

for file = Cars.jpgFiles';
    
    filename = strcat(Cars.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
        
    %Crop the image to by multiple of 8
    [h, w] = size(raw_image);
    w = floor(w/8)*8;
    h = floor(h/8)*8;
    raw_image = raw_image(1:h, 1:w);
    
    steg = image2steg(raw_image, message, bpp);
    
    decoded_message = decodeSteg(steg, bpp);
    
    %Calculate the MSE, PSNR, SSI
    Cars.MSE = [Cars.MSE mse(steg, raw_image)];
    Cars.PSNR = [Cars.PSNR PSNR(steg, raw_image, 255)];
    Cars.SSI = [Cars.SSI ssim(steg, raw_image)];        
end
Cars.MSE_avg = mean(Cars.MSE);
Cars.PSNR_avg = mean(Cars.PSNR);
Cars.SSI_avg = mean(Cars.SSI);

for file = Objects.jpgFiles';
    
    filename = strcat(Objects.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
    
    
    %Crop the image to by multiple of 8
    [h, w] = size(raw_image);
    w = floor(w/8)*8;
    h = floor(h/8)*8;
    raw_image = raw_image(1:h, 1:w);
    
    steg = image2steg(raw_image, message, bpp);
    
    decoded_message = decodeSteg(steg, bpp);
    
    %Calculate the MSE, PSNR, SSI
    Objects.MSE = [Objects.MSE mse(steg, raw_image)];
    Objects.PSNR = [Objects.PSNR PSNR(steg, raw_image, 255)];
    Objects.SSI = [Objects.SSI ssim(steg, raw_image)];
        
end
Objects.MSE_avg = mean(Objects.MSE);
Objects.PSNR_avg = mean(Objects.PSNR);
Objects.SSI_avg = mean(Objects.SSI);

% for file = Nature.jpgFiles';
%     
%     filename = strcat(Nature.jpgPath, file.name);  
%     raw_image = rgb2gray(imread(filename));
%       
%     %Crop the image to by multiple of 8
%     [h, w] = size(raw_image);
%     w = floor(w/8)*8;
%     h = floor(h/8)*8;
%     raw_image = raw_image(1:h, 1:w);
%     
%     steg = image2steg(raw_image, message, bpp);
%     
%     decoded_message = decodeSteg(steg, bpp);
%     
%     %Calculate the MSE, PSNR, SSI
%     Nature.MSE = [Nature.MSE mse(steg, raw_image)];
%     Nature.PSNR = [Nature.PSNR PSNR(steg, raw_image, 255)];
%     Nature.SSI = [Nature.SSI ssim(steg, raw_image)];     
% end
% Nature.MSE_avg = mean(Nature.MSE);
% Nature.PSNR_avg = mean(Nature.PSNR);
% Nature.SSI_avg = mean(Nature.SSI);

for file = People.jpgFiles';
    
    filename = strcat(People.jpgPath, file.name);  
    raw_image = rgb2gray(imread(filename));
     
    %Crop the image to by multiple of 8
    [h, w] = size(raw_image);
    w = floor(w/8)*8;
    h = floor(h/8)*8;
    raw_image = raw_image(1:h, 1:w);
    
    steg = image2steg(raw_image, message, bpp);
    
    decoded_message = decodeSteg(steg, bpp);
    
    %Calculate the MSE, PSNR, SSI
    People.MSE = [People.MSE mse(steg, raw_image)];
    People.PSNR = [People.PSNR PSNR(steg, raw_image, 255)];
    People.SSI = [People.SSI ssim(steg, raw_image)];        
end
People.MSE_avg = mean(People.MSE);
People.PSNR_avg = mean(People.PSNR);
People.SSI_avg = mean(People.SSI);

%% Plotting - MSE, PSNR and SSI with means

figure();
plot(Flowers.MSE);





