function [ y ] = mse( A, B )
% Calculates the mean squared error calculated between two images, A and B,
% which are of sizes X by Y.

if(size(A,1) ~= size(B,1) && size(A,2) ~= size(B,2) )
   fprintf('Input images must be of the same dimensions.');
   return; 
end

d = (A-B).^2;
res = sum(d(:));
y = 1/(size(A,1)*size(A,2))*res;


end

